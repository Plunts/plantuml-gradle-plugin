/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.plunts.gradle.plantuml.plugin;

import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.plugins.ExtensionContainerInternal;
import org.gradle.api.internal.project.ProjectInternal;
import org.gradle.api.internal.tasks.TaskContainerInternal;
import org.gradle.api.logging.Logger;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class PlantUmlPluginTest {

  @Test
  @SuppressWarnings("unchecked")
  void testApply() {
    ClassDiagramsExtension rootExtension = new ClassDiagramsExtension();
    rootExtension.setPlantumlServer("Foobar");
    rootExtension.setRenderClasspath(mock(FileCollection.class));
    ProjectInternal root = mock(ProjectInternal.class);

    ClassDiagramsExtension extension = new ClassDiagramsExtension();
    ProjectInternal intermediate = mock(ProjectInternal.class);
    ProjectInternal project = mock(ProjectInternal.class);
    when(project.getParent()).thenReturn(intermediate);
    when(intermediate.getParent()).thenReturn(root);

    Logger logger = mock(Logger.class);
    when(root.getLogger()).thenReturn(logger);
    when(intermediate.getLogger()).thenReturn(logger);
    when(project.getLogger()).thenReturn(logger);

    ExtensionContainerInternal extensionContainer = mock(ExtensionContainerInternal.class);
    when(extensionContainer.create(any(String.class), eq(ClassDiagramsExtension.class))).thenReturn(extension);
    when(extensionContainer.findByType(ClassDiagramsExtension.class)).thenReturn(null).thenReturn(rootExtension);
    when(root.getExtensions()).thenReturn(extensionContainer);
    when(intermediate.getExtensions()).thenReturn(extensionContainer);
    when(project.getExtensions()).thenReturn(extensionContainer);

    TaskContainerInternal taskContainer = mock(TaskContainerInternal.class);
    when(root.getTasks()).thenReturn(taskContainer);
    when(intermediate.getTasks()).thenReturn(taskContainer);
    when(project.getTasks()).thenReturn(taskContainer);

    new PlantUmlPlugin().apply(project);
    verify(extensionContainer).create("classDiagrams", ClassDiagramsExtension.class);
    verify(taskContainer).register("generateClassDiagrams", GenerateClassDiagramsTask.class, extension);
    assertThat(extension.getPlantumlServer(), is(rootExtension.getPlantumlServer()));
    assertThat(extension.getRenderClasspath(), is(rootExtension.getRenderClasspath()));
  }

}
